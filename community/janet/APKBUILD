# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=janet
pkgver=1.22.0
pkgrel=0
pkgdesc="Dynamic Lisp dialect and bytecode VM"
url="https://janet-lang.org/"
license="MIT"
arch="all !riscv64" # FTBS riscv64
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/janet-lang/janet/archive/v$pkgver.tar.gz"

# secfixes:
#   1.22.0-r0:
#     - CVE-2022-30763

prepare() {
	default_prepare

	case "$CARCH" in
	s390x)
		sed -i '/suite0009.janet/d' meson.build
		rm test/suite0009.janet
		;;
	esac
}

build() {
	make PREFIX=/usr
	make PREFIX=/usr build/janet.pc
	make PREFIX=/usr docs
}

check() {
	make test
}

package() {
	install -Dt "$pkgdir"/usr/bin build/janet
	install -Dm644 -t "$pkgdir"/usr/include/janet build/janet.h

	install -Dm644 build/libjanet.a -t "$pkgdir"/usr/lib/

	install -Dm755 build/libjanet.so "$pkgdir"/usr/lib/libjanet.so.$pkgver
	ln -s libjanet.so.$pkgver "$pkgdir"/usr/lib/libjanet.so
	ln -s libjanet.so.$pkgver "$pkgdir"/usr/lib/libjanet.so.${pkgver%.*.*}

	install -Dm644 build/janet.pc -t "$pkgdir"/usr/lib/pkgconfig/

	install -Dm644 -t "$pkgdir"/usr/share/man janet.1

	install -dm755 "$pkgdir"/usr/share/janet
	cp -a examples "$pkgdir"/usr/share/janet

	install -Dm644 build/doc.html "$pkgdir"/usr/share/doc/janet/doc.html
	install -Dm644 -t "$pkgdir"/usr/lib/janet tools/.keep
}

sha512sums="
c1e90dafa833618e0999a1a53005241b768dee66bf7e6d4cc60b7c7ce87fccba955ca2faafd4eacda320801667acbfc56087f423a4ad6abe7e97d1f08e136107  janet-1.22.0.tar.gz
"
