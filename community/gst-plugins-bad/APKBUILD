# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=gst-plugins-bad
pkgver=1.20.2
pkgrel=0
pkgdesc="GStreamer streaming media framework bad plug-ins"
url="https://gstreamer.freedesktop.org"
arch="all"
license="GPL-2.0-or-later LGPL-2.0-or-later"
options="!check" # 4 out of 51 tests fail as of 1.20.1
replaces="gst-plugins-bad1"
makedepends="alsa-lib-dev aom-dev bluez-dev bzip2-dev curl-dev directfb-dev faac-dev
	faad2-dev flite-dev glib-dev glu-dev gsm-dev gst-plugins-base-dev
	gstreamer-dev libass-dev libdc1394-dev libmms-dev libgudev-dev
	libmodplug-dev libsrtp-dev libvdpau-dev libwebp-dev libnice-dev
	libx11-dev mesa-dev meson neon-dev openssl1.1-compat-dev opus-dev orc-compiler
	orc-dev spandsp-dev tiff-dev x265-dev vulkan-loader-dev vulkan-headers
	wayland-dev wayland-protocols gobject-introspection-dev libusrsctp-dev
	lcms2-dev pango-dev chromaprint-dev fdk-aac-dev fluidsynth-dev
	libde265-dev openal-soft-dev openexr-dev openjpeg-dev libdvdnav-dev
	libdvdread-dev sbc-dev libsndfile-dev soundtouch-dev libxkbcommon-dev
	zbar-dev gtk+3.0-dev rtmpdump-dev vo-aacenc-dev vo-amrwbenc-dev
	"

case "$CARCH" in
	s390x|riscv64) ;;
	*) makedepends="$makedepends librsvg-dev libexif-dev" ;;
esac

subpackages="$pkgname-lang $pkgname-dev"
source="https://gstreamer.freedesktop.org/src/gst-plugins-bad/gst-plugins-bad-$pkgver.tar.xz"
ldpath="/usr/lib/gstreamer-1.0"

build() {
	abuild-meson \
		-Dpackage-origin="https://alpinelinux.org" \
		-Dpackage-name="GStreamer bad plug-ins (Alpine Linux)" \
		-Dintrospection=enabled \
		-Dsctp=enabled \
		-Dtests="$(want_check && echo enabled || echo disabled)" \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	# XDG_RUNTIME_DIR is just a temporary directory for a user, created
	# by pam_systemd (or in Alpine's case elogind's pam_elogind) mounted at
	# run/user/$(id -u), since we don't run elogind on the builders we can
	# just point it at /tmp
	XDG_RUNTIME_DIR=/tmp meson test -C output --print-errorlogs
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
3f98973dc07ead745418e0a30f9f6b5c8d328e3d126f54d92c10ab5da04271768a5c5dffc36ea24ccf8fb516b1e3733be9fb18dc0db419dea4d37d17018f8a70  gst-plugins-bad-1.20.2.tar.xz
"
