# Maintainer: Dave Henderson <dhenderson@gmail.com>
pkgname=gomplate
_goname="github.com/hairyhenderson/$pkgname"
pkgver=3.11.0
pkgrel=0
pkgdesc="A versatile Go template processor"
url="https://github.com/hairyhenderson/gomplate"
arch="all !s390x" # FAIL: TestCreateContext with SIGSEGV
license="MIT"
depends="ca-certificates"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz"
builddir="$srcdir/src/$_goname"

prepare() {
	mkdir -p "${builddir%/*}"
	mv "$srcdir"/$pkgname-$pkgver "$builddir"/

	default_prepare
}

build() {
	make build \
		GOPATH="$srcdir" \
		VERSION="$pkgver" \
		COMMIT="unknown"
}

check() {
	# Note: make test (that runs go test -race) doesn't work.
	GOPATH="$srcdir" go test -v
}

package() {
	install -D -m 755 bin/gomplate "$pkgdir"/usr/bin/gomplate
}

cleanup_srcdir() {
	[ -d src ] && chmod -R +w src
	default_cleanup_srcdir
}

sha512sums="ec327e0ade46975838090a615b9d17f43f520ad732301db46d76d6d3c1055e3d23e217a8e41636c147ddcb54d6111a226d49f9bc32d74e2de5305df65042d320  gomplate-3.11.0.tar.gz"
