# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=font-iosevka
pkgver=15.5.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/iosevka/iosevka-etoile.ttc
}

sha512sums="
4c041dcd95e77cba4e03b7a0a959d1b7d2175d8a1c5c268201c0ebdd52500e19b1aac5bb54f0c4c099f0d9724c82f2f41371c63e733025588d9114db29819c12  super-ttc-iosevka-15.5.0.zip
747ae5c0112b10e48c28105f8963635d06c916bfcc34a54db5f2fdb999679c1347793657af3b506a3eb190d1d050bd18ecadcbcd5e8e7349de4ac4255b244bc9  super-ttc-iosevka-slab-15.5.0.zip
4c455208f9be5b01877f0c0e3c7b80e4f4ff3c37bedcf51f75b0670c1a252f7999c7beaacfe519d7655b805d47aa1fbd8d7773e69a75ebabb3391621e2e6f249  super-ttc-iosevka-curly-15.5.0.zip
7a15489b68374994472b3ec45954dd5f61fe0b44f5e77c48b32553c2d95e24febcc3ec66e6912c760ae7977ffed96a59f17c9f430a260f0fe8cb0397f8e9c241  super-ttc-iosevka-curly-slab-15.5.0.zip
07ceb9d13b3e7cb405f68a417735e7892414211a7d9cbc2f862078a0700d311f2685538c47af9e2fb90c013d462c62bd4f2a735e143e6a70cd9b3c4b2b295279  super-ttc-iosevka-aile-15.5.0.zip
d9e5543c5de97d4f9b1932cb7bbafc69b340f5d82feb5a98241fdce0b29f5c70f82b1c3c183c6f79827cb7de512601a343d4fed074f427a6ef9e713ad4cf10b9  super-ttc-iosevka-etoile-15.5.0.zip
"
